

### index

# search in these labels to find related code block

### index
### printHelp
### parseArgs
### commonFunctions
### runCommands
### atCommands
### openSerialPort
### globalVars
### boot
### mainLoop


### printHelp


import re

def fdent(*texts):
  """\
  Formatted inDENT: enter multiline string (not f-string!) with optional variables,
  and get it back with leading spaces stripped according the *second* line,
  formatted with str.format() with additional positional arguments.
  i wrote this because dedent and others format string before strip spaces
  breaking the vertical alignment, as this function does the opposite it works ok
  """
  if len(texts) < 1:
    return ''
  lines = texts[0].splitlines()
  if len(lines) > 1:
    pre = re.search('^ *', lines[1])[0]
    return '\n'.join(line.removeprefix(pre).rstrip() for line in lines).format(*texts[1:])
  return lines[0]


import sys

if len(sys.argv) < 2 : # [0] is the command itself
  print(fdent("""\
    esp8266 serial simulator in python
    v6 sep 2021, gpl v3 atesin()gmail!com
    
    i wrote this just to learn python in a fun way so it may contain errors,
    i share it without any warranty thinking that *maybe* it could be useful,
    you are the solely responsibile for using it, if unsure don't do it

    description:
      it opens a serial port and simulates some responses like an esp8266.
      needs pySerial package installed ('pip install pyserial').
      reads a .save file in your home directory, if not found writes a default one.
      may also open tcp sockets if instructed to do so.
      it needs at least python v3.8 since it widely uses the walrus operator
    
    usage: esp-sim.py [serialPort]
      serialPort     : start listening the specified serial port.
      [no arguments] : print this help and exit.
      example        : 'esp-sim.py COM1'\
  """))
  exit()
   

### parseArgs


def die(msg):
  print(msg+', run without arguments for help')
  exit()


if sys.version_info < (3, 8):
  die('sorry, i need python v3.8 at least')
if len(sys.argv) > 2:
  die('Too many arguments, i am confused')
print('Starting esp8266 serial simulator...')


### commonFunctions


def initialize(restore=False):
  global flash, sram # global must be at top in a function
  if not restore: # atRestore() already restored factory settings and file
    print(f"loading default settings from '{flashFileName}'")
    try:
      flash['_DEF'] = eval('{'+(flashFile := open(flashFileName)).read()+'}') # be aware of code injection risk!
      flashFile.close()
    except FileNotFoundError:
      print("not found, creating a new one")
      restore = True
    except PermissionError:
      die("couldn't open the file for read")
    except SyntaxError as err:
      print('found syntax error at line', err.lineno)
      print(err.text+' '*(err.offset-1)+'^ here')
      die('was somebody playing with the file?')
    except NameError:
      die('missing expected configuration variable')
    except:
      die('error loading the file')
  if restore:
    saveFlash(True)
  flash['_CUR'] = flash['_DEF'].copy()
  
  # fix current values
  c = flash['_CUR']
  if not c['uart']['stopbits']:
    c['uart'] = {'baudrate':115200, 'bytesize':8, 'stopbits':1, 'parity':'N'}
  if c['cip_mac']['STA'] == 'ff:ff:ff:ff:ff:ff':
    c['cip_mac']['STA'] = '57:82:66:e5:82:66'
  if c['cip_mac']['AP'] == 'ff:ff:ff:ff:ff:ff':
    c['cip_mac']['AP'] = 'a9::82:66:e5:82:66'
  if not c['cwdhcps'][0]:
    c['cwdhcps'] = [120,'192.168.4.2','192.168.4.101']
  if not c['cipdns']:
    c['cipdns'] = ['208.67.222.222']
  
  serialSet('_CUR', c['uart'], False) # not yet listening
  sram = sramDefaults.copy()
  splash = fdent("""\
    ,--- ,--  ,--. ,--. ,--. ,--  ,--     ,--  . .  ,
    |--  `--. |--' ;--:  ,-' |--. |--. -- `--. | |\/|
    `---  --' '    `--' '--- `--' `--'     --' ' '  '
  """)
  ans("listening, press 'ctrl+c' to stop and exit", splash, 'ready')
  

def saveFlash(restore=False):
  global flash
  if restore:
    print(f"restoring 'factory' settings to '{flashFileName}'")
    flash['_DEF'] = flashDefaults.copy()
  else:      
    print(f"saving default settings to '{flashFileName}'")
  try:
    flashFile = open(flashFileName, mode='w')
    print(fdent("""\
      # esp8266 serial simulator default settings file
      # it is read on each (re)boot, please don't edit, instead please use *_DEF commands
      # it is read on (re)boots and overwritten on *_DEF or RESTORE commands
      
      {0}\
    """, ',\n'.join(repr(k)+' : '+repr(v) for k,v in flash['_DEF'].items())), file=flashFile)
    flashFile.close()
  except:
    die("couldn't save the file on the disk!, what a pity")
  

def serialInfo(target, u, set=False):
  """\
  get a serial configuration dict in pySerial format and returns as a friendly string
  target: '(_CUR|_DEF)', u: serial uart conf dict in pySerial format, set: true to set values, or just query'
  """
  values = f"{u['baudrate']} baud, {u['bytesize']}{u['parity']}{u['stopbits']}"
  return f"{targets[target]} serial port {'set to' if set else 'values'}: {sys.argv[1]}, {values}, no flow control"
  

def serialSet(target, uart, listening=True):
  """configures serial connection"""
  global flash
  bauds = (1200,2400,4800,9600,19200,38400,57600,115200)
  
  valuesOk = False
  if uart['baudrate'] in bauds:
    if uart['bytesize'] in (5,6,7,8):
      if uart['stopbits'] in (1,2):
        valuesOk = True
  if not valuesOk:
    log = fdent("""\
      to prevent conectivity loss i think this values are safer
      (you can change later for your real esp8266 at your risk):
      - baud rates: {0}
      - for arduino uno hardware 'Serial': {1}
      - data bits: from 5 to 8, stop bits: 1 or 2 (1, 3), flow control: off (0)\
    """, repr(bauds[:5]), bauds[5:])
    return ans(log, '', err)
  
  if listening:
    ans(serialInfo(target, uart, True), '', ok)
  else:
    print(serialInfo(target, uart, True))
    
  flash[target]['uart'] = uart
  if target == '_CUR':
    serialPort.apply_settings(uart)
  else:
    saveFlash()
  

def ans(log, msg, status):
  print(log)
  serialPort.write((msg+'\r\n'+status+'\r\n').encode())


def checkTarget(target):
  if target in targets:
    if target:
      return target
    print('this command is deprecated, please use *_CUR or *_DEF alternatives')
    return '_CUR'
  ans(badCmd, '', err)
  return ''


### runCommands


def runCmd(serialInput):
  global sram
  if sram['ate']:
    serialPort.write(serialInput.encode()) # serialInput already comes with '\r\n'
  print('>', (atCmd := serialInput.strip()))
  
  if not serialInput.endswith('\r\n'):
    ans('commands must end with <cr><lf> (ascii 13 and 10)', '', err)
  elif atCmd == 'AT':
    ans("i'm awake", '', ok)
  elif atCmd == 'ATE0':
    ans('disabling command echo', '', ok)
    sram['ate'] = False
  elif atCmd == 'ATE1':
    ans('enabling command echo', '', ok)
    sram['ate'] = True
  elif atCmd.startswith('AT+CIP'):
    runCipCmd(atCmd[6:])
  elif atCmd.startswith('AT+CW'):
    runCwCmd(atCmd[5:])
  elif atCmd.startswith('AT+SYS'):
    runSysCmd(atCmd[6:])
  elif atCmd.startswith('AT+'):
    runAtCmd(atCmd[3:])
  elif atCmd[0].islower():
    ans(badCmd+", remember all commands are uppercase", '', err)
  elif not atCmd.startswith('AT'):
    ans(badCmd+", remember most commands starts with 'AT+'", '', err)
  else:
    ans(badCmd, '', err)


def runAtCmd(atCmd):
  if   atCmd == 'GMR': atGmr()
  elif atCmd == 'RST': atRst()
  elif atCmd.startswith('UART_'): atUart(atCmd[4:8], atCmd[8:])
  elif atCmd.startswith('UART'): atUart('', atCmd[4:])
  elif atCmd == 'RESTORE': atRestore()
  else: ans(badCmd, '', err)
  
  
def runCipCmd(atCmd):
  if   atCmd == 'STATUS': cipStatus()
  elif atCmd.startswith('MODE'): cipMode(atCmd[4:])
  elif atCmd.startswith('MUX'): cipMux(atCmd[3:])
  elif atCmd.startswith('RECVMODE'): cipRecvmode(atCmd[8:])
  elif atCmd.startswith('APMAC_'): cip_mac('AP', atCmd[5:9], atCmd[9:])
  elif atCmd.startswith('APMAC'): cip_mac('AP', '', atCmd[5:])
  elif atCmd.startswith('STAMAC_'): cip_mac('STA', atCmd[6:10], atCmd[10:])
  elif atCmd.startswith('STAMAC'): cip_mac('STA', '', atCmd[6:])
  elif atCmd.startswith('AP_'): cip_('AP', atCmd[2:6], atCmd[6:])
  elif atCmd.startswith('AP'): cip_('AP', '', atCmd[2:])
  elif atCmd.startswith('STA_'): cip_('STA', atCmd[3:7], atCmd[7:])
  elif atCmd.startswith('STA'): cip_('STA', '', atCmd[3:])
  else: ans(badCmd, '', err)
  

def runCwCmd(atCmd):
  if False: pass
  elif atCmd.startswith('MODE_'): cwMode(atCmd[4:8], atCmd[8:])
  elif atCmd.startswith('MODE'): cwMode('', atCmd[4:])
  elif atCmd.startswith('DHCPS_'): cwDhcps(atCmd[5:9], atCmd[9:])
  elif atCmd.startswith('DHCP_'): cwDhcp(atCmd[4:8], atCmd[8:])
  elif atCmd.startswith('DHCP'): cwDhcp('', atCmd[4:])
  elif atCmd.startswith('JAP_'): cwJap(atCmd[3:7], atCmd[7:])
  elif atCmd.startswith('JAP'): cwJap('', atCmd[3:])
  elif atCmd.startswith('SAP_'): cwSap(atCmd[3:7], atCmd[7:])
  elif atCmd.startswith('SAP'): cwSap('', atCmd[3:])
  else: ans(badCmd, '', err)


def runSysCmd(atCmd):
  if False: pass
  else: ans(badCmd, '', err)
  
  
### atCommands


def atGmr():
  firmware = fdent("""\
    AT version:1.7.0.0(Apr 16 2019 15:31:04)
    SDK version:3.1.0-dev(3b41fcf)
    Compile time: Dec 26 2019 13:43:49
    ESP8266_AT_LoBo v1.3.1\
  """)
  ans('this simulator is inspired in ESP8266_AT_LoBo firmware', firmware, ok)


def atUart(target, param):
  if not (t := checkTarget(target)):
    return
  uart = flash[t]['uart']
  paritys = 'NOE' # 0, 1, 2
  
  # query serial parameters
  if param == '?':
    values = f"{uart['baudrate']},{uart['bytesize']},{uart['stopbits']},{'NOE'.find(uart['parity'])}"
    return ans(serialInfo(t, uart), f'+UART{target}:{values},0', ok)
  
  # validate serial parameters
  if (p := param[:1]) != '=':
    return ans(badSyntax, '', err)
  if p.lstrip('0123456789,'): # if remaining string has some length left (strcspn)
    return ans(badChars,'',err)
  if len(u := p.split(',')) != 5:
    return ans(badParams, '', err)

  # set serial parameters
  uart = {'baudrate': 0, 'bytesize': 0, 'stopbits': 0, 'parity': 'X'}
  if s[4] == '0':
    try:
      p = paritys[int(u[3])]
      uart = {'baudrate': int(u[0]), 'bytesize': int(u[1]), 'stopbits': int(u[2]), 'parity': p}
    except IndexError:
      pass
  serialSet(t, uart)
  if not target:
    serialSet('_DEF', uart)


def atRst():
  ans('Restarting simulator...', '', ok)
  initialize()

  
def atRestore():
  ans("Restoring 'factory' settings and restarting...", '', ok)
  initialize(True)


def cipMode(atCmd):
  global sram
  modes = ('normal','passthrough (transparent)')
  if atCmd == '?':
    return ans(f"currently in {modes[(m := sram['cipmode'])]} transmission mode", f"+CIPMUX:{m}", ok)
  if atCmd not in ('=0','=1'):
    return ans(badSyntax, '', err)
  ans(f"setting transmission mode to {modes[(m := int(atCmd[1]))]}", '', ok)
  sram['cipmode'] = m
  

def cipMux(atCmd):
  global sram
  modes = ('dis','en')
  if atCmd == '?':
    return ans(f"multiple connections are {modes[(m := sram['cipmux'])]}abled", f"+CIPMUX:{m}", ok)
  if atCmd not in ('=0','=1'):
    return ans(badSyntax, '', err)
  ans(f"{modes[(m := int(atCmd[1]))]}abling multiple connections", '', ok)
  sram['cipmux'] = m
  

def cipRecvmode(atCmd):
  global sram
  modes = ('act','pas')
  if atCmd == '?':
    return ans(f"tcp receive mode is {modes[(m := sram['ciprecvmode'])]}ive", f"+CIPRECVMODE:{m}", ok)
  if atCmd not in ('=0','=1'):
    return ans(badSyntax, '', err)
  ans(f"setting tcp mode as {modes[(m := int(atCmd[1]))]}ive", '', ok)
  sram['ciprecvmode'] = m


def cip_mac(mode, target, param):
  global flash
  if not (t := checkTarget(target)):
    return
  modes = {'AP':'SoftAP', 'STA':'Station'}
  
  if param == '?':
    return ans(f"{modes[mode]} {targets[t]} virtual mac address is {flash[t]['cip_mac'][mode]}", f"+CIP{mode}MAC{target}:\"{flash[t]['cip_mac'][mode]}\"", ok)
  if param[:1] != '=':
    return ans(badSyntax, '', err)
  if not re.search(r'^="[0-9a-f]{2}(:[0-9a-f]{2}){5}"$', (p := param.lower())):
    return ans(badParams, '', err)
  
  ans(f"setting {targets[target]} {modes[mode]} virtual mac address(es) as '{(mac := p[2:19])}'", '', ok)
  flash[t]['cip_mac'][mode] = mac
  if not target:
    flash['_DEF']['cip_mac'][mode] = mac
  if not target or t == '_DEF':
    saveFlash()


def cip_(mode, target, param): # mode AP|STA , target _CUR|_DEF|'' , param ?|=...
  global flash
  if not (t := checkTarget(target)):
    return
  modes = {'AP':'SoftAP', 'STA':'Station'}
  # query ip configuration
  if param == '?':
    noIp = ['0.0.0.0','0.0.0.0','0.0.0.0']
    if mode == 'AP' and flash[t]['cwmode'] == 1: # local ap not enabled
      cwmode = noIp
      log = f"{targets[t]} wi-fi is not in {modes[mode]} mode"
    elif mode == 'STA' and flash[t]['cwmode'] == 5: # not connected to a remote ap
      cwmode = noIp
      log = f"{targets[t]} wi-fi Station not connected to a remote AP"
    else:
      cwmode = flash[t]['cip_'][mode]
      log = fdent("""\
        actual ip configuration for {0} wi-fi {1}:
        - ip address, gateway, netmask
        - {2}, {3}, {4}\
      """, targets[t], modes[mode], cwmode[0], cwmode[1], cwmode[2])

    msg  = fdent('''\
      +CIP{0}{1}:ip:"{2}"
      +CIP{0}{1}:gateway:"{3}"
      +CIP{0}{1}:netmask:"{4}"\
    ''', mode, target, cwmode[0], cwmode[1], cwmode[2])
    return ans(log, msg, ok)
    
  # validate ip parameters, i explored many ways (split, filter, etc) but concluded this is maybe the best
  if param[:1] != '=':
    return ans(badCmd, '', err)
  if max(int(v) for v in re.findall(r'[0-9]+', '0'+param)) > 255:
    return ans(badParams, '', err)
  ip = r'"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"'
  if not (ips := re.findall(f'^=({ip})(,({ip})?)?(,({ip})?)?$', param)):
    return ans(badSyntax, '', err)
  i = lambda k: ips[0][k][1:-1]
  cip = [i(0), i(2) if i(2) else i(0), i(4) if i(4) else '255.255.255.0']
  
  # set ip configuration (pending: SoftAP and local dhcp server modifies each other)
  log = fdent("""\
    setting {0} {1} ip address(es):
    - ip address, gateway, netmask:
    - {2}, {3}, {4}\
  """, targets[t], modes[mode], cwmode[0], cwmode[1], cwmode[2])
  ans(log, '', ok)
  flash[t]['cip_'][mode] = cip
  if not target:
    flash['_DEF']['cip_'][mode] = cip
  if not target or t == '_DEF':
    saveFlash()


def cwDhcps(target, param):
  global flash
  # query settings
  if param == '?':
    msg = f"+CWDHCPS{target}:{','.join(flash[target]['cwdhcps'])}"
    return ans('printing {targets[target]} SoftAP dhcp server parameters', msg, ok)

  # reset settings to default ones: 120 lease time, next ip after local one, +99 ip addresses
  if param == '=0':
    localIp = flash[target]['cip_']['AP'][0]
    net = re.search('^(.*)[0-9]+$', localIp)[1]
    host = int(re.search('[0-9]+$', localIp)[0])
    addHost = lambda n: str(min(host+n, 255))
    fromHost = net+addHost(1)
    toHost = net+addHost(100)
    flash[target]['cwdhcps'] = [120, fromHost, toHost]
    log = fdent("""\
      {0} dhcp server defaults applied according local ip address:
      120 lease minutes, from ip {1}, to ip {2}\
    """, targets[target], fromHost, toHost)
    return ans(log, '', ok)

  # check set parameters, need all : =1,<lease>,"<start>","<end>"
  if param[:1] != '=':
    return ans(badCmd, '', err)
  if param[:3] != '=1,':
    return ans(badParams, '', err)
  ip = r'[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+'
  if not (p := re.findall(f'^=1,([0-9]+),("({ip})","({ip})")$', param)): # capture parameters
    return ans(badParams, '', err)
  p = p[0]
  if int(p[0]) > 2880:
    return ans(badParams, '', err)
  for oct in re.search(r'[0-9]+', p[1]):
    if int(oct) > 255:
      return ans(badParam, '', err)
  
  # set SoftAP dhcp server parameters
  log = fdent("""\
    setting {0} SoftAP dhcp server parameters:
    {1} lease minutes, from ip {2}, to ip {3}\
  """, targets[target], p[0], p[2], p[3])
  ans(log, '', ok)
  flash[target]['cwdhcps'] = [p[0], p[2], p[3]]
  if target == '_DEF':
    saveFlash()
  

def cwDhcp(target, param):
  global flash
  if not (t := checkTarget(target)):
    return
  
  # query dhcp server/client statuses
  if param == '?':
    return ans(f'dhcp statuses: bit0=SoftAP, bit1=Station', f"+CWDHCP{t}:{flash[t]['cwdhcp']}", ok)

  # validate set parameters
  if param[:1] != '=':
    return ans(badCmd, '', err)
  if not (p := re.findall(r'^=([012]),([01])$', param)):
    return ans(badParams, '', err)
  mode, en, cwdhcp = int(p[0][0])+1, int(p[0][1]), flash[t]['cwdhcp']
  cwdhcp |= mode # turn on selectively
  if not en: # if set disabled, turn off selectively
    cwdhcp ^= mode
    
  # set parameters
  ans(f"setting {targets[target]} dhcp status(es)", '', ok)
  flash[t]['cwdhcp'] = cwdhcp
  if not target:
    flash['_DEF']['cwdhcp'] = cwdhcp
  if not target or t == '_DEF':
    saveFlash()


def cwJap(target, param):
  global flash
  if not (t := checkTarget(target)):
    return

  # query parameters
  if param == '?':
    return ans("pending, printing dummy response 'No AP'", 'No AP', ok)
  
  # validate set parameters
  if param[:1] != '=':
    return ans(badCmd, '', err)
  # AT+CWJAP_DEF=<ssid>,<pwd>,[<bssid>][,<pci_en>]
  h = '[0-9a-fA-F]{2}'
  bssid = f'{h}:{h}:{h}:{h}:{h}:{h}'
  if not(p := re.search(f'^=".*?",".*?"(,"{bssid}")?(,[01])?$', param)): # just check format for now
    return ans(badParams, '', err)

  # set parameters
  ans("pending, printing dummy response 'FAIL'", f'+CWJAP{target}:3', 'FAIL')


def cwMode(target, param):
  global flash
  if not (t := checkTarget(target)):
    return
  wModes = ('','Station','SoftAP','Station+SoftAP')
  
  if param == '=?':
    return ans('wi-fi mode bitmask: bit0 = Station, bit1 = SoftAP', f"+CWMODE{target}:(1-3)", ok)
  if param == '?':
    return ans(f"{targets[t]} wi-fi mode is {wModes[(m := flash[t]['cwmode'])]}", f"+CWMODE{target}:{m}", ok)
  if param not in ('=1','=2','=3'):
    return ans(badSyntax, '', err)
  
  ans(f"setting {targets[target]} wi-fi mode(s) as {wModes[(p := int(param[1]))]}", '', ok)
  flash[t]['cwmode'] = p
  if not target:
    flash['_DEF']['cwmode'] = p
  if not target or t == '_DEF':
    saveFlash()


def cwSap(target, param):
  global flash
  if not (t := checkTarget(target)):
    return
  algos = ('open','','wpa psk','wpa2 psk','wpa+wpa2 psk')
  
  def apInfo(ap):
    return  fdent("""
      - ssid name, password, channel, encryption, connection slots, hidden
      - {0}, {1}, {2}, {3}, {4}, {5}\
    """, repr(ap[0]), repr(ap[1]), ap[2], algos[ap[3]], ap[4], 'yes' if ap[5] else 'no')
  
  # query actual parameters
  if param == '?':
    if flash[t]['cwmode'] == 1:
      return ans(f'{targets[t]} wifi is not in SoftAP mode', '', err)
    ap = flash[t]['cwsap']
    esc = lambda u: re.sub(r'([\\"])', r'\\\1', u) # better than str.translate()
    return ans(f'{targets[target]} SoftAP actual settings:{apInfo(ap)}', f'+CWSAP{target}:"{esc(ap[0])}","{esc(ap[1])}"'+','.join(str(v) for v in ap[2:]), ok)

  # set parameters validation
  if param[:1] != '=':
    return ans(badCmd, '', err)
  if not re.search(r'^=".*?(?<!\\)",".*?(?<!\\)",[0-9]+,[0-9]+(,[0-9]*)?(,[0-9]*)?$', param):
    return(badSyntax, '', err)
  if not (p := re.findall(r'^="(.{1,32}?)(?<!\\)","(|.{8,64}?)(?<!\\)",([1-9]|(1[0-4])),([0234])(,[1-8]?)?(,[01]?)?$', param)):
    return ans(badParams, '', err)

  p = p[0]
  unEsc = lambda e: re.sub(r'\\([\\"])', r'\1', e)
  intDefault = lambda s, d: int(s) if s else d
  #    ssid             pass            channel    encr_algo  max_cnx              hidden
  ap = [unEsc(p[0]), unEsc(p[1]), int(p[2]), int(p[4]), intDefault(p[5][:1], 4), intDefault(p[6][1:], 0)]
  if ap[4] and not ap[1]:
    return ans('with encryption enabled you need to supply a password', '', err)
  if ap[2] > 11:
    print('please notice ch12+ may not work in u.s. and ch14 may work in japan only')
  
  msg = fdent("""\
    new {0} SoftAP settings:
    - ssid name, password, channel, encryption, connection slots, hidden
    - {1}, {2}, {3}, {4}, {5}, {6}
  """, targets[target], repr(ap[0]), repr(ap[1]), ap[2], algos[ap[3]], ap[4], 'yes' if ap[5] else 'no')
  
  ans(f"applying new {targets[target]} SoftAP settings:{apInfo(ap)}", '', ok)
  flash[t]['cwsap'] = ap
  if not target:
    flash['_DEF']['cwsap'] = p
  if not target or t == '_DEF':
    saveFlash()
  

def cipStatus():
  ans("pending, for now it just says 'no wifi'", f"STATUS:5", ok)


### openSerialPort


try:
  import serial # pip install pyserial
  serialPort = serial.Serial(sys.argv[1], write_timeout=0) # no timeout = waits forever write ack if carrier disconnected
except ModuleNotFoundError:
  die('Package pySerial not installed')
except serial.serialutil.SerialException:
  die(f"Serial port '{sys.argv[1]}' not found or busy")
except:
  die('Error opening serial port')
  
  
### globalVars


# the main difference between flash and sram is on (re)boots...
# - flash is saved/restored to/from a file
# - sram is always resetted

sramDefaults = {
  'ate'      : True,
  'cipmux'      : 0,
  'ciprecvmode' : 0,
  'cipmode'     : 0
}

flashDefaults = {
  'uart'          : {'baudrate':0, 'bytesize':0, 'stopbits':0, 'parity':'N'},
  'cwdhcp'        : 3,
  'cip_mac'       : {'STA':'ff:ff:ff:ff:ff:ff', 'AP':'ff:ff:ff:ff:ff:ff'},
  'cip_'          : {'STA':['0.0.0.0','0.0.0.0','0.0.0.0'], 'AP':['0.0.0.0','0.0.0.0','0.0.0.0']},
  'cwdhcps'       : [0,'0.0.0.0','0.0.0.0'],
  'savetranslink' : 0,
  'cipdns'        : [],
  'sysmsg'        : '0x0',
  'cwcountry'     : [0,'CN',1,13],
  'cipsslcconf'   : 0,
  'cwmode'        : 2,
  'cwjap'         : [],
  'cwsap'         : ['ESP-E58266','',1,0,4,0],
  'cwautoconn'    : 1
}

targets = {'_CUR': 'current', '_DEF': 'default', '': 'both'}
modes = ('','Station','SoftAP','Station+SoftAP')
ok = 'OK'
err = 'ERROR'
deprecatedNotice = 'this command is deprecated, please use *_CUR or *_DEF alternatives'
badCmd = 'unrecognized or unsupported command'
badParams = 'invalid parameters'
badValue = 'value out of range'
badParamFormat = ''
badSyntax = 'bad syntax'
badChars = 'invalid characters found'


### boot


from os import getenv

flashFileName = getenv("HOME") or getenv("USERPROFILE") 
flashFileName += ('/' if '/' in flashFileName else '\\') + 'esp-sim.conf'
#flashFileName = 'esp-sim.flash'
flash = {}
sram = {}
initialize()
# use scheduler? (sched std lib)


### mainLoop


try:
  while True:
    if serialPort.in_waiting:
      serialInput = ''
      ch = ''
      while True:
        ch = serialPort.read().decode()
        serialInput += ch
        if ch == '\n' or not len(ch) or len(serialInput) > 79: # len 0 = timeout
          break
      runCmd(serialInput)
except KeyboardInterrupt:
  print('cancelled by user, closing and returning to console, bye')
#except:
#  die('Error when running')  
# NameError : name not defined (when reaches a var or func name that doesn't exist)

