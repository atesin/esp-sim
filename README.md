# esp-sim

esp8266 serial simulator in python

many commands will have to be simulated, like wi-fi or hardware related ones\
but in the future if i have enough time and energy i plan to add tcp sockets support to send/receive data if needed\
because i planned this software more like a tool to write network clients/servers for arduino
(AT+CIPSTART, AT+CIPSEND, AT+CIPSERVER, +IPD, AT+CIPRECVMODE, AT+CIPRECVDATA, etc.)
